DROP TABLE IF EXISTS LogAuction;
DROP TABLE IF EXISTS Auction;
DROP TABLE IF EXISTS Product;

CREATE TABLE Product
(
	Prod_ID		INT 		NOT NULL	AUTO_Increment,
	Prod_Name	VARCHAR(50)	NOT NULL,
	PRIMARY KEY (Prod_ID)
)Engine=InnoDB;

CREATE TABLE Auction
(
        AUC_ID          INT             NOT NULL,
        TimeStamp       DATETIME        NOT NULL,
        PROD_ID         INT             NOT NULL,
        Bid             FLOAT(20,2)     NOT NULL,
        PRIMARY KEY (AUC_ID),
        FOREIGN KEY (PROD_ID) REFERENCES Product(Prod_ID)
)Engine=InnoDB;

CREATE TABLE LogAuction
(
        Action          VARCHAR(10)     NOT NULL,
        AUC_ID          INT		NOT NULL,
        TimeStamp       DATETIME        NOT NULL,
        PROD_ID         INT             NOT NULL,
        Bid             FLOAT(20,2),
        FOREIGN KEY (PROD_ID) REFERENCES Product(PROD_ID)
)Engine=InnoDB;

INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Nayeon');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of jeongyeon');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Momo');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Sana');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Jihyo');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Mina');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Dahyun');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Chaeyoung');
INSERT INTO Product(PROD_ID, PROD_Name) VALUES(NULL, 'Picture of Tzuyu');
