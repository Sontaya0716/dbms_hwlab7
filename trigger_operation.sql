DROP TRIGGER IF EXISTS auction_operation;

DELIMITER $$

CREATE TRIGGER auction_operation
	AFTER INSERT ON LogAuction
	FOR EACH ROW
BEGIN
	DECLARE total FLOAT;

	IF (New.Bid > 0) THEN

		IF (New.Action = 'create') THEN
            INSERT INTO Auction(AUC_ID, TimeStamp, PROD_ID, Bid) VALUES(New.AUC_ID, New.TimeStamp, New.PROD_ID, New.Bid);
        	
		ELSEIF (New.Action = 'update') THEN
			SELECT Bid INTO total FROM Auction WHERE AUC_ID = New.AUC_ID;

			IF (New.Bid > total) THEN

				UPDATE Auction
                	SET TimeStamp = New.TimeStamp, Bid = New.Bid
                WHERE AUC_ID = New.AUC_ID
                	AND PROD_ID = New.PROD_ID;

        	END IF;

		ELSEIF (New.Action = 'delete') THEN
			SELECT Bid INTO total FROM Auction WHERE AUC_ID = New.AUC_ID;
            
            IF (New.Bid = total) THEN

				DELETE FROM Auction
                	WHERE AUC_ID = New.AUC_ID;

			END IF;

		END IF;

	END IF;
END $$
DELIMITER ;
